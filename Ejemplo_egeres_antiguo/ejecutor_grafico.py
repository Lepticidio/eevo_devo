import sys, random, math
import pygame
from pygame.locals import *
import pymunk
import pymunk.pygame_util
import pymunk.pyglet_util
#from pymunk.pyglet_util import draw
import pyglet
from pyglet.gl import *

import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation



starting_dna_a = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]
starting_dna_b = [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]


starting_dna_a = [51.76541827366837, 6.609695938474408, -81.68154293362673, 60.279277294367155, 26.21588397751841, -56.604547632450824, -28.758953153162384, -131.35676101739438, -165.95443478748, -60.55624720722972, 14.533977428562459, -226.62419812725057, -95.23473226239577, 25.839576494309295, -69.20549133606862, -149.21069540409417, -71.20219850007022, -8.831949645628384, -62.022754615695284, -91.78620789622389]
starting_dna_b = [-230.11215218716504, -37.25762362448971, 43.99360828327414, -144.9288578279838, -131.124231512981, 102.0888671204986, -91.65138083103636, 215.74903351140472, -73.4767461451101, -169.3187142154944, -142.9937630841425, 90.58256071651262, -7.521797911085019, -145.9459056287447, 2.459673661448157, -103.48566599914537, -205.9898038752089, 139.4905249084242, 170.8364188159719, -39.76336965235071]




fig = plt.figure()

config = pyglet.gl.Config(sample_buffers=1, samples=2, double_buffer=True)
window = pyglet.window.Window(1800, 900,config=config, vsync = False)

draw_options = pymunk.pyglet_util.DrawOptions()

#game_window = pyglet.window.Window(800, 600)


# Zooming constants
ZOOM_IN_FACTOR = 1.2
ZOOM_OUT_FACTOR = 1/ZOOM_IN_FACTOR

def update(dt):
    # Note that we dont use dt as input into step. That is because the
    # simulation will behave much better if the step size doesnt change
    # between frames.
    #r = 10
    #for x in range(r):
    #    space.step(1./30./r)

    space.step( 1/200.0)
    space2.step(1/200.0)
    space3.step(1/200.0)
    space4.step(1/200.0)
    space5.step(1/200.0)
    space6.step(1/200.0)
    space7.step(1/200.0)

    dora_evolucionadora()

batch = pyglet.graphics.Batch()

pyglet.clock.schedule_interval(update, 1/200.0)
fps_display = pyglet.clock.ClockDisplay()







global space
space = pymunk.Space()
space.gravity = (0.0, -900.0)

space2 = pymunk.Space()
space2.gravity = (0.0, -900.0)

space3 = pymunk.Space()
space3.gravity = (0.0, -900.0)

space4 = pymunk.Space()
space4.gravity = (0.0, -900.0)

space5 = pymunk.Space()
space5.gravity = (0.0, -900.0)

space6 = pymunk.Space()
space6.gravity = (0.0, -900.0)

space7 = pymunk.Space()
space7.gravity = (0.0, -900.0)


def add_ball(space):
    """Add a ball to the given space at a random position"""
    mass = 1
    radius = 14
    inertia = pymunk.moment_for_circle(mass, 0, radius, (0,0))
    body = pymunk.Body(mass, inertia)
    x = random.randint(120,380)
    body.position = x, 750
    shape = pymunk.Circle(body, radius, (0,0))
    space.add(body, shape)
    return shape




def add_L(space):
    """Add a inverted L shape with two joints"""
    #rotation_center_body = pymunk.Body(body_type = pymunk.Body.STATIC)
    rotation_center_body = pymunk.Body(body_type = pymunk.Body.STATIC)
    rotation_center_body.position = (300,300)

    #rotation_limit_body = pymunk.Body(body_type = pymunk.Body.STATIC)
    #rotation_limit_body.position = (200,300)


    #class pymunk.Body(mass=0, moment=0, body_type=<class 'CP_BODY_TYPE_DYNAMIC'>
    body = pymunk.Body(10, 10000)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body.position = (300,400)
    l1 = pymunk.Segment(body, (-150,     0),( 255.0,   0.0), 5)
    l2 = pymunk.Segment(body, (-150.0,   0),(-150.0,  50.0), 5)
    #l3 = pymunk.Segment(body, ( 255.0, 0.0),( 255.0, -10.0), 1)

    #rotation_center_joint = pymunk.PinJoint(body, rotation_center_body, (0,0), (0,0))
    #joint_limit = 25
    #rotation_limit_joint = pymunk.SlideJoint(body, rotation_limit_body, (-100,0), (0,0), 0, joint_limit)

    #space.add(l1, l2, body, rotation_center_joint, rotation_limit_joint)
    #space.add(l1, l2, body, rotation_center_joint)
    space.add(l1, l2, body)
    return l1,l2

def add_floor(space):
    body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body.position = (400,100)

    l1 = pymunk.Segment(body, (-300, 0), (300.0, 0.0), 4)

    space.add(l1, body)



def add_thing_1(space):
    pass






def main():
    global space

    global body1, body11
    global body2, body22
    global body3, body33
    global body4, body44
    global body5, body55
    global body6, body66
    global body7, body77

    global starting_dna_a, starting_dna_b

    #pygame.init()
    #screen = pygame.display.set_mode((1700, 900))
    #pygame.display.set_caption("Joints. Just wait and the L will tip over")
    #clock = pygame.time.Clock()

    space = pymunk.Space()
    space.gravity = (0.0, -900.0)

    #lines = add_L(space)
    #add_ball(space)
    #add_floor(space)

    t_y = -120

    static_lines = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)
                    #pymunk.Segment(space.static_body, (407.0, 246.0 + t_y), (407.0, 343.0 + t_y), 0.0)
                    ]
    for l in static_lines:
        l.friction = 0.5


    static_lines2 = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)]
    static_lines2[0].friction = 0.5

    static_lines3 = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)]
    static_lines3[0].friction = 0.5

    static_lines4 = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)]
    static_lines4[0].friction = 0.5

    static_lines5 = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)]
    static_lines5[0].friction = 0.5

    static_lines6 = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)]
    static_lines6[0].friction = 0.5

    static_lines7 = [ pymunk.Segment(space.static_body, (-4511.0, 246.0 + t_y),  (541407.0, 246.0 + t_y), 5.0)]
    static_lines7[0].friction = 0.5

    space.add(static_lines)
    space2.add(static_lines2)
    space3.add(static_lines3)
    space4.add(static_lines4)
    space5.add(static_lines5)
    space6.add(static_lines6)
    space7.add(static_lines7)

    #add_thing_1(space2)






    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body1 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body1.position = (356,300)
    shape = pymunk.Poly(body1, points)
    shape.friction = 1
    space.add(body1,shape)
    body11 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body11.position = (144,300)
    shape = pymunk.Poly(body11, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space.add(body11,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj1 = pymunk.PinJoint(body1, body11, (-100,0), (100,0))
    space.add(pj1)






    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body2 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body2.position = (356,300)
    shape = pymunk.Poly(body2, points)
    shape.friction = 1
    space2.add(body2,shape)
    body22 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body22.position = (144,300)
    shape = pymunk.Poly(body22, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space2.add(body22,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj2 = pymunk.PinJoint(body2, body22, (-100,0), (100,0))
    space2.add(pj2)





    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body3 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body3.position = (356,300)
    shape = pymunk.Poly(body3, points)
    shape.friction = 1
    space3.add(body3,shape)
    body33 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body33.position = (144,300)
    shape = pymunk.Poly(body33, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space3.add(body33,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj3 = pymunk.PinJoint(body3, body33, (-100,0), (100,0))
    space3.add(pj3)






    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body4 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body4.position = (356,300)
    shape = pymunk.Poly(body4, points)
    shape.friction = 1
    space4.add(body4,shape)
    body44 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body44.position = (144,300)
    shape = pymunk.Poly(body44, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space4.add(body44,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj4 = pymunk.PinJoint(body4, body44, (-100,0), (100,0))
    space4.add(pj4)







    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body5 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body5.position = (356,300)
    shape = pymunk.Poly(body5, points)
    shape.friction = 1
    space5.add(body5,shape)
    body55 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body55.position = (144,300)
    shape = pymunk.Poly(body55, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space5.add(body55,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj5 = pymunk.PinJoint(body5, body55, (-100,0), (100,0))
    space5.add(pj5)










    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body6 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body6.position = (356,300)
    shape = pymunk.Poly(body6, points)
    shape.friction = 1
    space6.add(body6,shape)
    body66 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body66.position = (144,300)
    shape = pymunk.Poly(body66, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space6.add(body66,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj6 = pymunk.PinJoint(body6, body66, (-100,0), (100,0))
    space6.add(pj6)








    size = 10
    #points = [(-size * 2.4, -size * 0.3), (-size * 2.4, size * 0.3), (size * 2.4,size * 0.3), (size * 2.4, -size * 0.3)]
    points = [(-size* 10, -size * 1.5), (-size* 10, size * 1.5), (size* 10,size * 1.5), (size* 10, -size * 1.5)]
    mass = 1.0
    moment = pymunk.moment_for_poly(mass, points, (0,0))
    body7 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body7.position = (356,300)
    shape = pymunk.Poly(body7, points)
    shape.friction = 1
    space7.add(body7,shape)
    body77 = pymunk.Body(mass, moment)
    #body = pymunk.Body(body_type = pymunk.Body.STATIC)
    body77.position = (144,300)
    shape = pymunk.Poly(body77, points)
    shape.friction = 1
    shape.filter = pymunk.ShapeFilter(group = 2)
    space7.add(body77,shape)

    #pj = pymunk.PinJoint(space.static_body, body, (-50,300), (0,0))
    pj7 = pymunk.PinJoint(body7, body77, (-100,0), (100,0))
    space7.add(pj7)









    #body2 = pymunk.Body(mass, moment)
    #body2 = pymunk.Body(body_type = pymunk.Body.STATIC)
    #body2.position = (200,200)
    #shape = pymunk.Poly(body2, points)
    #shape.friction = 1
    #shape.filter = pymunk.ShapeFilter(group = 1)
    #space.add(body2,shape)



    #draw_options = pymunk.pygame_util.DrawOptions(screen)

    global count
    global flag
    global total_flag

    count      = 0
    flag       = 0
    total_flag = 0

    global lista_0a
    global lista_1a
    global lista_2a
    global lista_3a
    global lista_4a
    global lista_5a
    global lista_6a
    global lista_7a
    global lista_0b
    global lista_1b
    global lista_2b
    global lista_3b
    global lista_4b
    global lista_5b
    global lista_6b
    global lista_7b

    lista_0a = starting_dna_a[:]
    lista_1a = starting_dna_a[:]
    lista_2a = starting_dna_a[:]
    lista_3a = starting_dna_a[:]
    lista_4a = starting_dna_a[:]
    lista_5a = starting_dna_a[:]
    lista_6a = starting_dna_a[:]
    lista_7a = starting_dna_a[:]

    lista_0b = starting_dna_b[:]
    lista_1b = starting_dna_b[:]
    lista_2b = starting_dna_b[:]
    lista_3b = starting_dna_b[:]
    lista_4b = starting_dna_b[:]
    lista_5b = starting_dna_b[:]
    lista_6b = starting_dna_b[:]
    lista_7b = starting_dna_b[:]




    #evolver
    lista_listas = [lista_0a,lista_1a,lista_2a,lista_3a,lista_4a,lista_5a,lista_6a,lista_7a,
                    lista_0b,lista_1b,lista_2b,lista_3b,lista_4b,lista_5b,lista_6b,lista_7b]

    numero_cambios   = 10
    numero_cambios_2 = 10

    for i in lista_listas:
        for j in range(numero_cambios):
            index_dowm = random.randint(0,19)
            i[index_dowm] = i[index_dowm] + random.uniform(-13.0, 13.0)

    while False:
        for event in pygame.event.get():
            if event.type == QUIT:
                sys.exit(0)
            elif event.type == KEYDOWN and event.key == K_ESCAPE:
                sys.exit(0)
            elif event.type == KEYDOWN and event.key == K_p:
                pygame.image.save(screen, "slide_and_pinjoint.png")


            elif event.type == KEYDOWN and event.key == K_SPACE:
                #s = pymunk.DampedRotarySpring(body1, body2, 0.15, 20000000,900000)
                #space.add(pj, s)
                body1.angular_velocity = -50







        #count = count - 1



        if flag < 25:
            flag = flag + 1
        else:
            flag = 0

            count = count + 1

            if count == 20:
                count = 0

            #print count
            body1.angular_velocity  = lista_0a[count]
            body11.angular_velocity = lista_0b[count]

            body2.angular_velocity  = lista_1a[count]
            body22.angular_velocity = lista_1b[count]

            body3.angular_velocity  = lista_2a[count]
            body33.angular_velocity = lista_2b[count]

            body4.angular_velocity  = lista_3a[count]
            body44.angular_velocity = lista_3b[count]

            body5.angular_velocity  = lista_4a[count]
            body55.angular_velocity = lista_4b[count]

            body6.angular_velocity  = lista_5a[count]
            body66.angular_velocity = lista_5b[count]

            body7.angular_velocity  = lista_6a[count]
            body77.angular_velocity = lista_6b[count]

        total_flag = total_flag + 1
        if total_flag == 50 * 30:
            total_flag = 0
            print "resetting..."
            print (body1.position.x + body11.position.x) * 0.5
            print (body2.position.x + body22.position.x) * 0.5
            print (body3.position.x + body33.position.x) * 0.5
            print (body4.position.x + body44.position.x) * 0.5
            print (body5.position.x + body55.position.x) * 0.5
            print (body6.position.x + body66.position.x) * 0.5
            print (body7.position.x + body77.position.x) * 0.5
            listaaa = [
                [1   ,  (body1.position.x + body11.position.x) * 0.5 ],
                [2   ,  (body2.position.x + body22.position.x) * 0.5 ],
                [3   ,  (body3.position.x + body33.position.x) * 0.5 ],
                [4   ,  (body4.position.x + body44.position.x) * 0.5 ],
                [5   ,  (body5.position.x + body55.position.x) * 0.5 ],
                [6   ,  (body6.position.x + body66.position.x) * 0.5 ],
                [7   ,  (body7.position.x + body77.position.x) * 0.5 ]
            ]

            listaaa.sort(key=lambda x: x[1])

            print listaaa

            if listaaa[-1][0]   == 1:
                winner_a = lista_0a
                winner_b = lista_0b

            elif listaaa[-1][0] == 2:
                winner_a = lista_1a
                winner_b = lista_1b

            elif listaaa[-1][0] == 3:
                winner_a = lista_2a
                winner_b = lista_2b

            elif listaaa[-1][0] == 4:
                winner_a = lista_3a
                winner_b = lista_3b

            elif listaaa[-1][0] == 5:
                winner_a = lista_4a
                winner_b = lista_4b

            elif listaaa[-1][0] == 6:
                winner_a = lista_5a
                winner_b = lista_5b

            elif listaaa[-1][0] == 7:
                winner_a = lista_6a
                winner_b = lista_6b
            #print winner_a
            #print winner_b

            lista_0a = winner_a[:]
            lista_1a = winner_a[:]
            lista_2a = winner_a[:]
            lista_3a = winner_a[:]
            lista_4a = winner_a[:]
            lista_5a = winner_a[:]
            lista_6a = winner_a[:]
            lista_7a = winner_a[:]

            lista_0b = winner_b[:]
            lista_1b = winner_b[:]
            lista_2b = winner_b[:]
            lista_3b = winner_b[:]
            lista_4b = winner_b[:]
            lista_5b = winner_b[:]
            lista_6b = winner_b[:]
            lista_7b = winner_b[:]

            merged = winner_a + winner_b




            body1.position  = (356,300)
            body11.position = (144,300)

            body2.position  = (356,300)
            body22.position = (144,300)

            body3.position  = (356,300)
            body33.position = (144,300)

            body4.position  = (356,300)
            body44.position = (144,300)

            body5.position  = (356,300)
            body55.position = (144,300)

            body6.position  = (356,300)
            body66.position = (144,300)

            body7.position  = (356,300)
            body77.position = (144,300)

            lista_listas = [lista_0a,lista_1a,lista_2a,lista_3a,lista_4a,lista_5a,lista_6a,lista_7a,
                            lista_0b,lista_1b,lista_2b,lista_3b,lista_4b,lista_5b,lista_6b,lista_7b]

            for i in lista_listas:

                print "CHANGING..."
                print i

                for j in range(13):
                    index_dowm = random.randint(0,19)
                    i[index_dowm] = i[index_dowm] + random.uniform(-13.0, 13.0)

                print i

            print "\n\n\n"
            for k in lista_listas:
                print k




        space.step(1/50.0)
        space2.step(1/50.0)
        space3.step(1/50.0)
        space4.step(1/50.0)
        space5.step(1/50.0)
        space6.step(1/50.0)
        space7.step(1/50.0)

        screen.fill((255,255,255))

        space.debug_draw(draw_options)
        space2.debug_draw(draw_options)
        space3.debug_draw(draw_options)
        space4.debug_draw(draw_options)
        space5.debug_draw(draw_options)
        space6.debug_draw(draw_options)
        space7.debug_draw(draw_options)



        pygame.display.flip()
        clock.tick(50)

if __name__ == '__main__':
    main()











hugh_mungus = 0


def dora_evolucionadora():
    global count
    global flag
    global total_flag
    global hugh_mungus

    global lista_0a
    global lista_1a
    global lista_2a
    global lista_3a
    global lista_4a
    global lista_5a
    global lista_6a
    global lista_7a
    global lista_0b
    global lista_1b
    global lista_2b
    global lista_3b
    global lista_4b
    global lista_5b
    global lista_6b
    global lista_7b

    if flag < 25:
        flag = flag + 1
    else:
        flag = 0

        count = count + 1
        if count == 20:
            count = 0
        #print count
        body1.angular_velocity  = lista_0a[count]
        body11.angular_velocity = lista_0b[count]

        body2.angular_velocity  = lista_1a[count]
        body22.angular_velocity = lista_1b[count]

        body3.angular_velocity  = lista_2a[count]
        body33.angular_velocity = lista_2b[count]

        body4.angular_velocity  = lista_3a[count]
        body44.angular_velocity = lista_3b[count]
        body5.angular_velocity  = lista_4a[count]
        body55.angular_velocity = lista_4b[count]
        body6.angular_velocity  = lista_5a[count]
        body66.angular_velocity = lista_5b[count]
        body7.angular_velocity  = lista_6a[count]
        body77.angular_velocity = lista_6b[count]
    total_flag = total_flag + 1
    if total_flag == 50 * 60:
        total_flag = 0
        print "resetting...    lastest result : ",
        #print (body1.position.x + body11.position.x) * 0.5
        #print (body2.position.x + body22.position.x) * 0.5
        #print (body3.position.x + body33.position.x) * 0.5
        #print (body4.position.x + body44.position.x) * 0.5
        #print (body5.position.x + body55.position.x) * 0.5
        #print (body6.position.x + body66.position.x) * 0.5
        #print (body7.position.x + body77.position.x) * 0.5
        listaaa = [
            [1   ,  (body1.position.x + body11.position.x) * 0.5 ],
            [2   ,  (body2.position.x + body22.position.x) * 0.5 ],
            [3   ,  (body3.position.x + body33.position.x) * 0.5 ],
            [4   ,  (body4.position.x + body44.position.x) * 0.5 ],
            [5   ,  (body5.position.x + body55.position.x) * 0.5 ],
            [6   ,  (body6.position.x + body66.position.x) * 0.5 ],
            [7   ,  (body7.position.x + body77.position.x) * 0.5 ]
        ]
        listaaa.sort(key=lambda x: x[1])

        print listaaa[-1]
        #print listaaa
        if listaaa[-1][0]   == 1:
            winner_a = lista_0a
            winner_b = lista_0b
        elif listaaa[-1][0] == 2:
            winner_a = lista_1a
            winner_b = lista_1b
        elif listaaa[-1][0] == 3:
            winner_a = lista_2a
            winner_b = lista_2b
        elif listaaa[-1][0] == 4:
            winner_a = lista_3a
            winner_b = lista_3b
        elif listaaa[-1][0] == 5:
            winner_a = lista_4a
            winner_b = lista_4b
        elif listaaa[-1][0] == 6:
            winner_a = lista_5a
            winner_b = lista_5b
        elif listaaa[-1][0] == 7:
            winner_a = lista_6a
            winner_b = lista_6b
        #print winner_a
        #print winner_b
        #merged = winner_a + winner_b

        #plt.plot( range(len(merged)), merged, linewidth=2.0)
        #plt.grid(True)
        #plt.show()



        f, axarr = plt.subplots(2, sharex=True)

        axarr[0].plot(range(len(winner_a)), winner_a, color="k")
        axarr[0].set_title('A and B')
        axarr[0].fill_between(range(len(winner_a)), 0, winner_a,facecolor='green', alpha=0.2)
        axarr[0].axis([0, 19, -40, 40])

        axarr[1].plot(range(len(winner_b)), winner_b, color="k")
        axarr[1].fill_between(range(len(winner_b)), 0, winner_b,facecolor='red', alpha=0.2)
        axarr[1].axis([0, 19, -40, 40])

        #plt.axis([0, 19, -40, 40])
        plt.savefig('iteration_'+str(hugh_mungus)+'.png')
        hugh_mungus = hugh_mungus + 1

        lista_0a = winner_a[:]
        lista_1a = winner_a[:]
        lista_2a = winner_a[:]
        lista_3a = winner_a[:]
        lista_4a = winner_a[:]
        lista_5a = winner_a[:]
        lista_6a = winner_a[:]
        lista_7a = winner_a[:]
        lista_0b = winner_b[:]
        lista_1b = winner_b[:]
        lista_2b = winner_b[:]
        lista_3b = winner_b[:]
        lista_4b = winner_b[:]
        lista_5b = winner_b[:]
        lista_6b = winner_b[:]
        lista_7b = winner_b[:]

        body1.position  = (356 + 500,400)
        body1.angle  = 0
        body1.velocity  = (0.,0.)
        body11.position = (144 + 500,400)
        body11.angle  = 0
        body11.velocity  = (0.,0.)

        body2.position  = (356 + 500,400)
        body2.angle  = 0
        body2.velocity  = (0.,0.)
        body22.position = (144 + 500,400)
        body22.angle  = 0
        body22.velocity  = (0.,0.)

        body3.position  = (356 + 500,400)
        body3.angle  = 0
        body3.velocity  = (0.,0.)
        body33.position = (144 + 500,400)
        body33.angle  = 0
        body33.velocity  = (0.,0.)

        body4.position  = (356 + 500,400)
        body4.angle  = 0
        body4.velocity  = (0.,0.)
        body44.position = (144 + 500,400)
        body44.angle  = 0
        body44.velocity  = (0.,0.)

        body5.position  = (356 + 500,400)
        body5.angle  = 0
        body5.velocity  = (0.,0.)
        body55.position = (144 + 500,400)
        body55.angle  = 0
        body55.velocity  = (0.,0.)

        body6.position  = (356 + 500,400)
        body6.angle  = 0
        body6.velocity  = (0.,0.)
        body66.position = (144 + 500,400)
        body66.angle  = 0
        body66.velocity  = (0.,0.)

        body7.position  = (356 + 500,400)
        body7.angle  = 0
        body7.velocity  = (0.,0.)
        body77.position = (144 + 500,400)
        body77.angle  = 0
        body77.velocity  = (0.,0.)

        lista_listas = [lista_0a,lista_1a,lista_2a,lista_3a,lista_4a,lista_5a,lista_6a,lista_7a,
                        lista_0b,lista_1b,lista_2b,lista_3b,lista_4b,lista_5b,lista_6b,lista_7b]
        for i in lista_listas:
            #print "CHANGING..."
            #print i
            for j in range(10):
                index_dowm = random.randint(0,19)
                i[index_dowm] = i[index_dowm] + random.uniform(-9.0, 9.0)
            #print i
        #print "\n\n\n"
        #for k in lista_listas:
        #    print k


















@window.event
def on_key_press(symbol, modifiers):
    if symbol == pyglet.window.key.P:
        pyglet.image.get_buffer_manager().get_color_buffer().save("pyglet_util_demo.png")
    elif symbol == pyglet.window.key.A:
        glScalef( 0.9, 0.9, 1 )
    elif symbol == pyglet.window.key.D:
        glScalef( 1.1, 1.1, 1 )
    elif symbol == pyglet.window.key.E:
        plt.show()

@window.event
def on_draw():
    pyglet.gl.glClearColor(240,240,240,255)
    window.clear()

    fps_display.draw()

    #gluPerspective(60,1,0,10)

    # static attach points
    #pyglet.gl.glColor3f(1,0,1)
    #pyglet.gl.glPointSize(6)
    #a = []
    #for b in static_bs:
    #    a += [b.position.x, b.position.y]
    #    pyglet.graphics.draw(len(a)//2, pyglet.gl.GL_POINTS, ('v2f',a))

    # web crossings / bodies
    #pyglet.gl.glColor3f(.8,.8,.8)
    #a = []
    #for b in bs:
    #    a += [b.position.x, b.position.y]
    #pyglet.gl.glPointSize(4)
    #pyglet.graphics.draw(len(a)//2, pyglet.gl.GL_POINTS, ('v2f',a))


    # web net / constraints
    #a = []
    #for j in space.constraints:
    #    a += [j.a.position.x, j.a.position.y, j.b.position.x, j.b.position.y]
    #    pass

    #pyglet.graphics.draw(len(a)//2, pyglet.gl.GL_LINES, ('v2f',a))

    # anything else
    space.debug_draw( draw_options)
    space2.debug_draw(draw_options)
    space3.debug_draw(draw_options)
    space4.debug_draw(draw_options)
    space5.debug_draw(draw_options)
    space6.debug_draw(draw_options)
    space7.debug_draw(draw_options)

pyglet.app.run()
