from eevo_lib import eevo
import random


# x = random.randint(-400, 400)
# y = random.randint(-400, 400)
# eevo.Cell_0((500+x, 500+y))

# x = random.randint(-400, 400)
# y = random.randint(-400, 400)
x = random.randint(-4  , 4  )
y = random.randint(-4  , 4  )
eevo.Cell_0((500+x, 500+y))


# eevo.Cell_0((500, 500))
# eevo.Cell_0((530, 500))

# def add(dt):
#     x = random.randint(-300, 300)
#     y = random.randint(-300, 300)
#     eevo.Cell_0((500+x, 500+y))
# eevo.eevo_emulation.pyglet.clock.schedule_interval(add, 5)

def update_frame(dt):
    eevo.update_cells_array(dt)

eevo.start_simulation(update_frame, fullscreen=True)
