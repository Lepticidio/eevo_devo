
"""Showcase what the output of pymunk.pyglet_util draw methods will look like.
See pygame_util_demo.py for a comparison to pygame.
"""

__docformat__ = "reStructuredText"

import sys
import pyglet
import pymunk
import pymunk.pyglet_util
import random
from   pymunk.vec2d import Vec2d


window = pyglet.window.Window(1000, 1000, vsync=False)
space = pymunk.Space()
space.gravity = (0.0, 10.0)
space.damping = 0
draw_options = pymunk.pyglet_util.DrawOptions()


mass = 0.1
radius = 75
inertia = pymunk.moment_for_circle(mass, 0, radius, (0, 0))
body = pymunk.Body(mass, inertia)
# ||||||||||||||||||||||||||||||||||||||||||||
shape = pymunk.Circle(body, radius, (500,500))
shape.elasticity = 0.98
shape.friction = 0
shape.color = (230, 50, 14, 255)
space.add(body, shape)


mass = 0.1
radius = 75
inertia = pymunk.moment_for_circle(mass, 0, radius, (0, 0))
body = pymunk.Body(mass, inertia)
# ||||||||||||||||||||||||||||||||||||||||||||
shape = pymunk.Circle(body, radius, (550,500))
shape.elasticity = 0.98
shape.friction = 0
space.add(body, shape)


def add_cell(dt):
    mass = 0.1
    radius = 75
    inertia = pymunk.moment_for_circle(mass, 0, radius, (0, 0))
    body = pymunk.Body(mass, inertia)
    # ||||||||||||||||||||||||||||||||||||||||||||
    x = random.randint(-320, 320)
    y = random.randint(-320, 320)
    shape = pymunk.Circle(body, radius, (550+x,500+y))
    shape.elasticity = 0.98
    shape.friction = 0
    shape.color = (
        random.randint(120, 255),
        random.randint(120, 255),
        random.randint(120, 255),
        255)

    space.add(body, shape)

def update(dt):
    space.step( 1/200.0)

batch = pyglet.graphics.Batch()

pyglet.clock.schedule_interval(update, 1/200.0)
pyglet.clock.schedule_interval(add_cell, 5)

# otherwise save screenshot wont work
_ = pyglet.clock.ClockDisplay()

@window.event
def on_draw():
    pyglet.gl.glClearColor(255,255,255,255)
    window.clear()
    space.debug_draw(draw_options)
    # textbatch.draw()

@window.event
def on_key_press(symbol, modifiers):
    if symbol == pyglet.window.key.P:
        pyglet.image.get_buffer_manager().get_color_buffer().save("pyglet_util_demo.png")


pyglet.app.run()
