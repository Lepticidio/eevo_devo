#!/usr/bin/env python
# -*- coding: utf-8 -*-

import math
import random
import eevo_emulation

# important public methods and variables !
list_cells = []

def start_simulation(input_function=None, fullscreen=False):
    eevo_emulation.start_simulation(input_function, fullscreen)

def update_cells_array(dt):
    for i in list_cells:
        i.update(dt)



# classes !

class Cell_0():
    """Cell_0"""

    def __init__(self, input_position):

        if type(input_position) != type( (0,0) ):
            raise Exception("You must feed in a tuple as first parameter to indicate de position of the cell !")

        # propiedades químicas
        self.comida = 25
        self.thresold_duplicarse = 60
        self.ratio_absorcion = 6 # 8 unidades por cada segundo
        self.time_counter = 0
        self.lifetime = 15

        self.start_physics(input_position)

        list_cells.append(self)


    def update(self, dt):
        self.time_counter += dt
        self.lifetime     -= dt

        if self.lifetime <= 0:
            eevo_emulation.space.remove(self.shape, self.shape.body)
            for n, i in enumerate(list_cells):
                if i == self:
                    del list_cells[n]
                    break
            return

        if self.time_counter >= 1:
            self.comida      += self.ratio_absorcion
            self.time_counter = 0
        if (self.thresold_duplicarse <= self.comida):
            self.comida = 25
            self.duplicate(25)

        self.radius = self.comida
        self.shape.unsafe_set_radius(self.radius)


    def duplicate(self, food, angle=None, vel=100):
        if (angle == None):
            angle = random.uniform(0.0, math.pi*2)
        self.body.velocity = math.cos(angle)*vel, math.sin(angle)*vel
        angle = -angle
        last = Cell_0((self.body.position.x, self.body.position.y))
        angle = random.uniform(0.0, math.radians(2))
        last.body.velocity = math.cos(angle)*vel, math.sin(angle)*vel

    def start_physics(self, input_position):
        self.mass    = 0.1
        self.radius  = self.comida
        self.inertia = eevo_emulation.pymunk.moment_for_circle(self.mass, 0, self.radius, (0, 0))
        self.body    = eevo_emulation.pymunk.Body(self.mass, self.inertia)
        self.body.position = input_position[0], input_position[1]
        self.shape   = eevo_emulation.pymunk.Circle(self.body, self.radius, (0, 0))
        self.shape.elasticity = 0.8
        self.shape.friction = 0.3
        self.shape.color = (
        random.randint(120, 255),
        random.randint(120, 255),
        random.randint(120, 255),
        255)
        eevo_emulation.space.add(self.body, self.shape)

    def recalculate_volume(self):
        pass







#
