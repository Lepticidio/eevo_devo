import sys
import pyglet
import random
import pymunk
import pymunk.pyglet_util
from   pymunk.vec2d import Vec2d


space = pymunk.Space()
space.gravity = 0.0, 0.0
space.damping = 0.3
draw_options = pymunk.pyglet_util.DrawOptions()


def update(dt):
    space.step( 1/200.0)

batch = pyglet.graphics.Batch()

pyglet.clock.schedule_interval(update, 1/200.0)
# pyglet.clock.schedule_interval(add_cell, 5)

# otherwise save screenshot wont work
_ = pyglet.clock.ClockDisplay()


def start_simulation(input_function=None, fullscreen=False):

    if fullscreen:
        window = pyglet.window.Window(fullscreen = True)
    else:
        window = pyglet.window.Window(1000, 1000, vsync=False)


    @window.event
    def on_draw():
        pyglet.gl.glClearColor(255,255,255,255)
        window.clear()
        space.debug_draw(draw_options)
        # textbatch.draw()

    @window.event
    def on_key_press(symbol, modifiers):
        if symbol == pyglet.window.key.P:
            pyglet.image.get_buffer_manager().get_color_buffer().save("pyglet_util_demo.png")

    if input_function != None:
        pyglet.clock.schedule_interval(input_function, 1/200.0)
    pyglet.app.run()





# esto da igual, pero no se borra de momento, pls
if __name__ == "__main__":
    # window = pyglet.window.Window(1000, 1000, vsync=False)
    # space = pymunk.Space()
    # space.gravity = (0.0, 10.0)
    # space.damping = 0.05


    mass = 10
    radius = 25
    inertia = pymunk.moment_for_circle(mass, 0, radius, (0,0))
    body = pymunk.Body(mass, inertia)
    # x = random.randint(115,350)
    body.position = 500, 700
    shape = pymunk.Circle(body, radius, (0,0))
    shape.elasticity = 0.95
    shape.friction = 0.9
    space.add(body, shape)

    vel = 100
    shape.body.velocity = random.randrange(-vel, vel), random.randrange(-vel, vel)

    avel = 50
    shape.body.angular_velocity = random.randrange(-avel, avel)

    start_simulation()



    print "hayyy !"
